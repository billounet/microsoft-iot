#include "stdafx.h"
#include "arduino.h"
#include "Adafruit_GFX.h"    // Core graphics library
#include "Adafruit_ST7735.h" // Hardware-specific library
#include <SPI.h>

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
//Use these pins for the shield!
#define sclk 13 // LCD SPI Clock
#define mosi 11 // LCD SPI Data line
#define cs   10 // LCD Chipselect
#define dc   8	// LCD Data/Command line
#define rst  0  // LCD Reset line

//-----------------------------------------------------------------------------
// Local functions
//-----------------------------------------------------------------------------
void testdrawtext(char *text, uint16_t color);


//-----------------------------------------------------------------------------
// Global
//-----------------------------------------------------------------------------
Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, rst);


//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
// Helper function for logging to debug output and the console
void CustomLogging(char* str)
{
	OutputDebugStringA(str); // for VS Output
	printf(str); // for commandline output
}

int _tmain(int argc, _TCHAR* argv[])
{
	return RunArduinoSketch();
}


void setup()
{
	// If your TFT's plastic wrap has a Black Tab, use the following:
	tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
	// If your TFT's plastic wrap has a Red Tab, use the following:
	//tft.initR(INITR_REDTAB);   // initialize a ST7735R chip, red tab
	// If your TFT's plastic wrap has a Green Tab, use the following:
	//tft.initR(INITR_GREENTAB); // initialize a ST7735R chip, green tab

	CustomLogging("Start Blue fill");

	tft.fillScreen(ST7735_BLUE);
	delay(500);

	// large block of text
	tft.fillScreen(ST7735_BLACK);
	testdrawtext("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur adipiscing ante sed nibh tincidunt feugiat. Maecenas enim massa, fringilla sed malesuada et, malesuada sit amet turpis. Sed porttitor neque ut ante pretium vitae malesuada nunc bibendum. Nullam aliquet ultrices massa eu hendrerit. Ut sed nisi lorem. In vestibulum purus a tortor imperdiet posuere. ", ST7735_WHITE);
	delay(1000);

}

void loop()
{
	Sleep(1000);
}

void testdrawtext(char *text, uint16_t color) {
	tft.setCursor(0, 0);
	tft.setTextColor(color);
	tft.setTextWrap(true);
	tft.print(text);
}

// Main.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "arduino.h"
#include "RTClib.h"


// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;
RTC_DS1307 rtc;
DateTime dt;
char outputBuffer[1024];

// Helper function for logging to debug output and the console
void CustomLogging(char* str)
{
	OutputDebugStringA(str); // for VS Output
	printf(str); // for commandline output
}

int _tmain(int argc, _TCHAR* argv[])
{
	return RunArduinoSketch();
}

void setup()
{
	// Initialize the I2C (TwoWire) library
	Wire.begin();

	CustomLogging("Hello Heart Beat!\n");
	// initialize the digital pin as an output.
	pinMode(led, OUTPUT);

	// Set a default time
	rtc.adjust(DateTime(2014, 8, 22, 12, 00, 59));
}

void loop()
{
	CustomLogging("LED ON\n");
	digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
	delay(1000);               // wait for a second
	CustomLogging("LED OFF\n");
	digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
	delay(1000);               // wait for a second
	
	// Get time from the RTC
	dt = rtc.now();
	sprintf_s(outputBuffer, "%d:%02d:%02d\n", dt.hour(), dt.minute(), dt.second());
	OutputDebugStringA(outputBuffer);
}


